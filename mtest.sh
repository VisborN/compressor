# if [ "$#" -ne 1 ]; then
#     echo "Illegal number of parameters"
#     exit 1
# fi
make
status=$?
if [ $status -ne 0 ]; then
    exit $status
fi
# cd build
# make
# status=$?
# if [ $status -ne 0 ]; then
#     exit $status
# fi
# cd ../
exec 3>&1
if [ "$silence" != "" ]; then
    exec 3>/dev/null
fi
for fullname in "$@"
do
    fname="$(basename -- $fullname)"
    ./build/compress --mode c --method ari --input "$fullname" --output "out/$fname.comp"   1>&3

    status=$?
    if [ $status -ne 0 ]; then
        exit $status
    fi
    if [ "$deco" = "" ]; then
    

        ./build/compress --mode d --method ari --input "out/$fname.comp" --output "out/$fname.decomp" 1>&3
    

        status=$?
        if [ $status -ne 0 ]; then
            exit $status
        fi

        if cmp  "$fullname" "out/$fname.decomp" ; then
            printf 'The file "%s" compressed successfully ' "$fullname"
            FILESIZE1=$(stat -c%s "$fullname")
            FILESIZE2=$(stat -c%s "out/$fname.comp")
            if [ $FILESIZE1 -ne 0 ]; then 
                echo  "print('compression level %.9f sizes %10d %10d'\
                %($FILESIZE2/$FILESIZE1, $FILESIZE1, $FILESIZE2))" | python3 
            else
                echo to zero
            fi
            #rm "$2" "out/$fname.decomp"
        else
            # head -c -12 "$fullname"
            echo "-------------------------------------------------------------------" 
            # head -c -12 "out/$fname.decomp"
            echo "-------------------------------------------------------------------" 
            exit 1 
        fi
    fi
done
