IMAGE=compressor_tests:latest
binary=build/compress
objects=build/objects
src=src
CC=gcc
CFLAGS=-O2 -pedantic -Wall -Werror -std=gnu11 -Werror=unused-result \
		-g -Wcast-align -Wshift-overflow=2 -Wcast-qual -Wduplicated-cond \
		-fno-omit-frame-pointer -fsanitize=address -fsanitize=undefined \
		-Wlogical-op -Wformat=2 -Wmissing-field-initializers
# CFLAGS=-Ofast
		
LDFLAGS=-lm
.ONESHELL:
.SILENT:
.SUFFIXES:

all: $(binary)

SRCS:=$(wildcard $(src)/*.c) $(wildcard $(src)/*.cpp)
OBJECTNAMES:=$(addprefix $(objects)/, $(addsuffix .o, $(SRCS)))


$(binary): $(OBJECTNAMES)
	@mkdir -p $(@D)
	$(CC) $(LDFLAGS) $(CFLAGS) $^ -o $@


define OBJECTS_template =
$(1): $(shell gcc -MM $(2) | sed -e s/^.*://)
	@mkdir -p $(dir $(1))
	@echo "$(shell gcc -MM $(2) | sed -e s/^.*://)"
	$(CC) -c $(CFLAGS) $(2) -o $(1)  
endef
$(foreach OBJ,$(OBJECTNAMES),$(eval $(call OBJECTS_template,$(OBJ), $(OBJ:$(objects)/%.o=%))))



# Testing
test: $(binary)
	python3 tests/run.py $(out)

build:
	docker build . -t $(IMAGE)

no-cache:
	docker build . -t $(IMAGE) --no-cache

run: build
	docker run $(IMAGE)

sh: build
	docker run -it $(IMAGE) /bin/sh

# Base image stuff

base-build:
	docker build . \
		-f Dockerfile.base \
		-t grihabor/compressor_base:latest

base-push:
	docker push grihabor/compressor_base:latest

base-build-and-push: base-build base-push

.PHONY: all build run no-cache sh base-build base-push base-build-and-push mtest 

testFiles=$(sort $(wildcard test_files/*))
out=1>/dev/null
method=ari
tl=180s
rmr:
	echo "method,file,original_size,compressed_size,conclusion" > results.csv

mtest: $(binary)
	mkdir -p out
	sum=0
	for FILE in $(testFiles)
	do
		NAME="$$(basename -- "$$FILE")"
		command="./build/compress --method $(method) --mode "
		F2="out/$$NAME.comp"
		F3="out/$$NAME.decomp"
		FS1="$$(stat -c%s "$$FILE")"; 
		timeout --foreground $(tl) $$command c --input "$$FILE" --output "$$F2" $(out); 
		st=$$?
		if [ $$st -eq 124 ]; then
			echo "$(method),$$NAME,$$FS1,0,TL1"| tee -a results.csv; continue;
		fi
		if [ $$st -ne 0 ]; then
			echo "$(method),$$NAME,$$FS1,0,RE1" | tee -a results.csv; continue;
		fi
ifndef deco
		timeout --foreground $(tl) $$command d --input "$$F2" --output "$$F3" $(out); 
		st=$$?
		if [ $$st -eq 124 ]; then
			echo "$(method),$$NAME,$$FS1,0,TL2" | tee -a results.csv; continue;
		fi
		if [ $$st -ne 0 ]; then
			echo "$(method),$$NAME,$$FS1,0,RE2" | tee -a results.csv; continue;
		fi
		FS2="$$(stat -c%s $$F2)"; 
		cmp  "$$FILE" "$$F3";
		sum=$$(($$sum + $$FS2))
		if [ $$? -ne 0 ]; then
			echo "$(method),$$NAME,$$FS1,$$FS2,WA" | tee -a results.csv; continue;
		else
			printf "$(method),$$NAME,";
			if [ $$FS1 -ne 0 ];
			then python3 -c "print('\033[96m%.5f, %-7d to %d\033[0m'%($$FS2/$$FS1, $$FS1, $$FS2))";
			else python3 -c "print('\033[96m       , %-7d to %d\033[0m'%( $$FS1, $$FS2))"; fi
			echo "$(method),$$NAME,$$FS1,$$FS2,OK" >> results.csv;
		fi

endif
	done
	printf "sum of all %d\n" $$sum

zip:
	for i in $(testFiles); do rm out/$$(basename $$i).zip; zip out/$$(basename $$i).zip $$i; done;


rar:
	for i in $(testFiles); do rm out/$$(basename $$i).rar; rar a out/$$(basename $$i).rar $$i; done;


# define OBJECTS_template =
# $(1): $(shell gcc -MM $(2) | sed -e s/^.*://)
# 	@mkdir -p $(dir $(1))
# 	@echo "$(shell gcc -MM $(2) | sed -e s/^.*://)"
# 	$(CC) -E $(CFLAGS) $(2) -o $(1)  
# endef
# $(foreach OBJ,$(OBJECTNAMES),$(eval $(call OBJECTS_template,$(OBJ:%.o=%), $(OBJ:$(objects)/%.i.o=%))))

# $(objects)/%.i.o: $(objects)/%.i
# 	@mkdir -p $(dir $@)
# 	$(CC) -c $(CFLAGS) $< -o $@
