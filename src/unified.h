#pragma once
#include "mfileutils.h"
#include <inttypes.h>
typedef struct Table_
{
    int64_t count;
    int64_t missed;
    int64_t aggressive;
    uint64_t maxDiv;
    uint64_t zero;
    uint64_t t[257];
    uint64_t divider;
    struct Table_ *prev;
    struct Table_ *next[257];
} Table;
typedef struct Context_
{
    Table *table_;
    int tableCount;
    int tableCountI;
    int tableSize;
    Table *table;
    uint64_t zero;
    uint64_t shadowed[257];
    uint64_t *t;
    uint64_t a;
    uint64_t b;
    uint64_t val;
    FILE_IO files;
    int bits_to_follow;
    int backInd;
    int context[10];
    int byte_in;
    int decompress;
    uint64_t ONE;
    uint64_t HALF;
    uint64_t FIRST_QTR;
    uint64_t THIRD_QTR;
} Context;

void getInInterval(Context *c);
void add_bits_plus_follow(Context *c, int bit);
int getByteFromValue(Context *c);