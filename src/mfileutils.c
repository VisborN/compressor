#include "mfileutils.h"
#include <errno.h>
#include <string.h>
int mopen(FILE_IO *c, char *ifile, char *ofile)
{

    c->fi = (FILE *)fopen(ifile, "rb");
    if (c->fi == NULL) {
        fprintf(stderr, "Error opening %s: %s\n", ifile, strerror(errno));
        return -1;
    }
    c->fo = (FILE *)fopen(ofile, "wb");
    if (c->fo == NULL) {
        fprintf(stderr, "Error opening %s: %s\n", ofile, strerror(errno));
        fclose(c->fi);
        return -1;
    }
    return 0;
}

void readTable(FILE_IO *c, uint64_t *table)
{
    if (fread(table, 1, 256, c->fi) == 0) {
        table[0] = 1;
    }
}
void getbestTable(FILE_IO *c, uint64_t *table)
{
    int byte = 0;
    uint64_t count[256] = {0};
    while ((byte = getByte(c)) != -1) {
        count[byte]++;
    }
    for (int i = 0; i < 256; i++) {
        count[i]++;
        table[i] = count[i] + table[i - 1];
        printf("%x %lu ", i, table[i]);
    }
    printf("\n");
    int add = 0;
    for (int i = 0; i < 256; i++) {
        table[i] = table[i] * table[-2] / table[255] + add;
        if (count[i] != 0 && table[i] == table[i - 1]) {
            table[i] += 1;
            add++;
        }
        if (table[i] <= table[i - 1]) {
            printf("bad\n");
        }
        printf("%x %lu ", i, table[i]);
    }
    printf("\n");
    // uint64_t sum = 0;
    // sum = table[255] - table[-2];
    // table[255] = table[-2];
    // for (int i = 254; i >= 0; i--) {
    //     table[i] -= sum;
    //     if (table[i] >= table[i - 1]) {
    //         sum--;
    //     }
    // }
    // for (int i = 0; i < 256; i++) {
    //     printf("%x %lu ", i, table[i]);
    // }
    // printf("\n");
    fseek(c->fi, 0, SEEK_SET);
    fwrite(table, 1, 256, c->fo);
}
uint64_t getfileLen(FILE *f)
{
    fseek(f, 0L, SEEK_END);
    uint64_t file_size = ftell(f);
    fseek(f, 0, SEEK_SET);
    return file_size;
}
int mclose(FILE_IO *c)
{
    mflush(c);
    fclose(c->fi);
    fclose(c->fo);
    return 0;
}
int getBit(FILE_IO *c)
{

    if (c->buff_i == c->buff_len) {
        c->buff_len = fread(c->buff, 1, sizeof(c->buff), c->fi);
        c->buff_i = 0;
        c->buff_i_bit = 0;
    }
    if (c->buff_i < c->buff_len) {
        int ret = c->buff[c->buff_i] & (0x80 >> c->buff_i_bit) ? 1 : 0;
        c->buff_i_bit++;
        c->buff_i += c->buff_i_bit / 8;
        c->buff_i_bit %= 8;
        return ret;
    } else {
        return -1;
    }
}
int getByte(FILE_IO *c)
{

    if (c->buff_i == c->buff_len) {
        c->buff_len = fread(c->buff, 1, sizeof(c->buff), c->fi);
        c->buff_i = 0;
        c->buff_i_bit = 0;
    }
    if (c->buff_i < c->buff_len) {
        uint8_t ret = c->buff[c->buff_i];
        c->buff_i++;
        return ret;
    } else {
        return -1;
    }
}
void writeByte(FILE_IO *c, uint8_t byte)
{
    c->buff2[c->buff2_i] = byte;
    c->buff2_i_bit = 0;
    c->buff2_i++;
    if (c->buff2_i == sizeof(c->buff2)) {
        mflush(c);
    }
}
void mflush(FILE_IO *c)
{
    if (c->buff2_i_bit > 0) {
        c->buff2_i++;
    }
    if (c->buff2_i > 0) {
        fwrite(c->buff2, c->buff2_i, 1, c->fo);
        c->buff2_i_bit = 0;
        c->buff2_i = 0;
    }
}
void writeBit(FILE_IO *c, int bit)
{
    if (c->buff2_i_bit == 0) {
        c->buff2[c->buff2_i] = 0;
    }
    const uint64_t con = ((uint64_t)1 << (sizeof(c->buff2[c->buff2_i]) * 8 - 1));
    c->buff2[c->buff2_i] |= bit ? con >> c->buff2_i_bit : 0;
    c->buff2_i_bit++;
    if (c->buff2_i_bit == sizeof(c->buff2[c->buff2_i]) * 8) {
        c->buff2_i_bit = 0;
        c->buff2_i++;
        if (c->buff2_i == sizeof(c->buff2)) {
            mflush(c);
        }
    }
}