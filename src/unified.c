
#include "unified.h"
#include <stdlib.h>

void add_bits_plus_follow(Context *c, int bit)
{
    if (!c->decompress) {
        int not_first = 0;
        do {
            writeBit(&c->files, bit ^ not_first);
            not_first = 1;
            c->bits_to_follow--;
        } while (c->bits_to_follow >= 0);
        c->bits_to_follow = 0;
    }
}

static void writeComp(Context *c);
void getInInterval(Context *c)
{

    uint64_t delitel = c->t[c->tableSize - 1];
    uint64_t *t = c->t;
    if (t[c->byte_in - 1] >= t[c->byte_in]) {

        for (int i = 0; i < c->tableSize; i++) {
            fprintf(stderr, "%lu ", t[i]);
        }
        fprintf(stderr, "\n");
        fprintf(stderr, "Error t[c->byte_in - 1] == t[c->byte_in] %x %ld %ld\n", c->byte_in,
                t[c->byte_in], t[c->byte_in - 1]);
        exit(1);
    }
    uint64_t in1 = t[c->byte_in - 1];
    uint64_t in2 = t[c->byte_in];
    uint64_t tl = c->a + in1 * (c->b - c->a + 1) / delitel;
    uint64_t tr = c->a - 1 + in2 * (c->b - c->a + 1) / delitel;

    c->a = tl;
    c->b = tr;
    writeComp(c);
}

int getByteFromValue(Context *c)
{
    int i = 0;
    uint64_t *t = c->t;
    while ((c->val - c->a) >= ((c->b - c->a + 1) * t[i]) / t[c->tableSize - 1]) {
        i++;
    }
    return i;
}

static void writeComp(Context *c)
{
    // printf("comp %.30lf %.30lf\n", c->a, c->b);
    for (;;) {
        // printf("got %g %g\n", c->a, c->b);
        if (c->a >= c->HALF) {
            c->val -= c->HALF;
            c->a -= c->HALF;
            c->b -= c->HALF;
            // printf("comp out %.30lf %.30lf\n", c->a, c->b);
            // printf("to1 %.30lf %.30lf\n", c->a / (double)ONE, (c->b + 1) / (double)ONE);
            add_bits_plus_follow(c, 1);
        } else if (c->b < c->HALF) {

            // printf("comp out %.30lf %.30lf\n", c->a, c->b);
            // printf("to1 %.30lf %.30lf\n", c->a / (double)ONE, (c->b + 1) / (double)ONE);
            add_bits_plus_follow(c, 0);
        } else if (c->a >= c->FIRST_QTR && c->b < c->THIRD_QTR) {
            // printf("\nhere\n");
            c->bits_to_follow++;

            c->val -= c->FIRST_QTR;
            c->a -= c->FIRST_QTR;
            c->b -= c->FIRST_QTR;
            // printf("to1 %.30lf %.30lf\n", c->a / (double)ONE, (c->b + 1) / (double)ONE);
        } else
            break;
        c->a += c->a;
        c->b += c->b + 1;
        // printf("to %.30lf %.30lf %08lx %08lx\n", c->a / (double)ONE, (c->b + 1) / (double)ONE,
        // c->a,
        //        c->b);
        if (c->a >= c->ONE - 1) {
            printf("\n\nto %.30lf %.30lf %08lx %08lx\n", c->a / (double)c->ONE,
                   (c->b + 1) / (double)c->ONE, c->a, c->b);
            exit(1);
        }
        if (c->decompress) {
            int in = getBit(&c->files);
            if (in != -1) {
                c->val += c->val + in;
            } else {
                // printf("end\n");
                c->val += c->val;
            }
        }
    }
    // printf("\033[0;31mto %.30lf %.30lf\033[0m\n", c->a / (double)ONE, (c->b + 1) / (double)ONE);
}