#include "ari.h"
#include "mfileutils.h"
#include "unified.h"
#include <errno.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void updateTable(Context *c)
{

    double maxFreq = 0;
    Table *maxFreqTable = c->table;
    int maxFreqTableI = 0;
    for (int j = 0; j < c->tableCount; j++) {
        Table *table = &c->table_[j];
        uint64_t *t = table->t;
        // printf("%ld\n", table[c->byte_in] - table[c->byte_in - 1]);
        if ((t[c->byte_in] - t[c->byte_in - 1]) / (double)t[c->tableSize - 1] > maxFreq) {
            maxFreq = (t[c->byte_in] - t[c->byte_in - 1]) / (double)t[c->tableSize - 1];
            maxFreqTable = c->table_ + j;
            maxFreqTableI = j;
        }
        if (table->aggressive >= 0) {

            for (int i = c->byte_in; i < c->tableSize; i++) {
                t[i] += table->aggressive;
            }
            if (t[c->tableSize - 1] >= table->maxDiv) {

                uint64_t add = 0;
                for (int i = 0; i < c->tableSize; i++) {
                    t[i] >>= 1;
                    t[i] += add;
                    if (t[i] == t[i - 1]) {
                        t[i]++;
                        add++;
                    }
                }
            }
        } else {
            uint64_t add = t[c->byte_in] - t[c->byte_in - 1] + table->aggressive > 1
                               ? table->aggressive
                               : -t[c->byte_in] + t[c->byte_in - 1] + 1;
            for (int i = c->byte_in; i < c->tableSize; i++) {
                t[i] += add;
            }
            if (t[c->tableSize - 1] < table->maxDiv / 2) {

                for (int i = 0; i < c->tableSize; i++) {
                    t[i] *= 2;
                }
            }
        }
    }
    // for (int j = 0; j < c->tableCount; j++) {
    //     if (maxFreqTableI != j) {
    //         c->table_[j].count = 0;

    //     } else {
    //         c->table_[j].count++;
    //     }
    // }
    // if (c->table_[maxFreqTableI].count > 10 && c->table != maxFreqTable) {
    //     // printf("%x changed table to %ld\n", c->byte_in, maxFreqTable - c->table_[0]);
    //     c->table = maxFreqTable;
    // }

    int maxTableCount = 0;
    for (int j = 0; j < c->tableCount; j++) {
        if (maxFreqTableI != j) {
            if (c->table_[j].count > 0) {
                c->table_[j].count--;
            }

        } else {
            c->table_[j].count++;
            // if (c->table_[j].count > 100) {
            //     c->table_[j].count = 100;
            // }
        }
        if (c->table_[j].count > maxTableCount) {
            maxTableCount = c->table_[j].count;
        }
    }
    if (c->table_[maxFreqTableI].count == maxTableCount && c->table != maxFreqTable) {
        // printf("%x changed table to %ld\n", c->byte_in, maxFreqTable - c->table_[0]);
        c->table = maxFreqTable;
    }
    c->t = c->table->t;
    // if (c->missed > 100) {
    //     c->table = maxFreqTable;
    // }

    // for (int i = 0; i < tableSize; i++) {
    //     printf("%lu ", c->table_[1][i]);
    // }
    // printf("\n");
}
void initContext(Context *c)
{
    const uint64_t MAXDIV = ((uint64_t)1 << 12);
    const uint64_t AGGRESSIVE = 256;
    c->ONE = ((uint64_t)1 << 32);
    c->HALF = (c->ONE / 2);
    c->FIRST_QTR = (c->ONE / 4);
    c->THIRD_QTR = (c->FIRST_QTR * 3);
    c->tableCount = 5;
    c->tableSize = sizeof(c->table_[0].t) / sizeof(c->table_[0].t[0]);
    c->table_ = calloc(c->tableCount, sizeof(c->table_[0]));
    c->table = &c->table_[0];
    c->t = c->table->t;
    c->a = 0;
    c->b = c->ONE - 1;
    for (int j = 0; j < c->tableCount; j++) {
        Table *table = &c->table_[j];
        table->aggressive = 1 << (c->tableCount - (j));
        table->maxDiv = MAXDIV;

        for (int i = 0; i < c->tableSize; i++) {
            table->t[i] = (i + 1);
        }
    }
    // c->table_[0].maxDiv = MAXDIV;
    // c->table_[0].aggressive = AGGRESSIVE << 1;
    c->table_[1].maxDiv = MAXDIV;
    c->table_[1].aggressive = AGGRESSIVE;

    c->table_[1].maxDiv = MAXDIV;
    c->table_[1].aggressive = 1 << 4;
    // for (int i = 0; i < c->tableSize; i++) {
    //     c->table_[1].t[i] = (i + 1) * (MAXDIV) / c->tableSize;
    // }

    c->table_[2].maxDiv = MAXDIV << 14;
    c->table_[2].aggressive = AGGRESSIVE;

    c->table_[3].maxDiv = MAXDIV << 6;
    c->table_[3].aggressive = AGGRESSIVE >> 3;
    c->table_[4].maxDiv = MAXDIV >> 3;
    c->table_[4].aggressive = AGGRESSIVE >> 6;
}

void compress_ari(char *ifile, char *ofile)
{
    Context c_ = {0};
    Context *c = &c_;
    if (mopen(&c->files, ifile, ofile)) {
        return;
    }
    initContext(c);

    int in;
    while ((in = getByte(&c->files)) != -1) {
        c->byte_in = in;
        // printf("%c", c->byte_in);
        getInInterval(c);
        updateTable(c);
    }
    c->byte_in = 256;
    // printf("%c", c->byte_in);
    getInInterval(c);
    updateTable(c);
    if (c->a != 0) {
        add_bits_plus_follow(c, 1);
    } else {
        add_bits_plus_follow(c, 0);
    }
    printf("%g %g\n", c->a / (double)c->ONE, c->b / (double)c->ONE);

    mclose(&c->files);
    free(c->table_);
}
void decompress_ari(char *ifile, char *ofile)
{
    Context c_ = {0};
    Context *c = &c_;
    if (mopen(&c->files, ifile, ofile)) {
        return;
    }
    c->decompress = 1;
    initContext(c);
    int in = 0;
    if (in != -1) {
        for (int i = 8; c->ONE >> i > 0 && in != -1; i += 8) {
            in = getByte(&c->files);
            if (in != -1) {
                c->val |= (uint64_t)in * (c->ONE >> i);
            }
        }
        for (;;) {
            int dec = getByteFromValue(c);
            if (dec == -1 || dec == 256) {
                break;
            }
            c->byte_in = dec;
            getInInterval(c);
            updateTable(c);

            writeByte(&c->files, c->byte_in);
        }
    }

    mclose(&c->files);
    free(c->table_);
}
