
#include "mfileutils.h"
#include "unified.h"
#include <stdio.h>
#include <stdlib.h>

#include "ppm.h"

void freeRecoursive(Table *t)
{
    for (int i = 0; i < 257; i++) {

        if (t->next[i]) {
            freeRecoursive(t->next[i]);
            free(t->next[i]);
        }
    }
}
void printRecoursive(Table *t)
{
    for (int i = 0; i < 257; i++) {
        if (*(t->t + i) - *(t->t + i - 1)) {
            printf("%d=%lu ", i, *(t->t + i) - *(t->t + i - 1));
        }
    }
    printf("\n");
    for (int i = 0; i < 257; i++) {

        if (t->next[i]) {
            printf("to %d ", i);
            printRecoursive(t->next[i]);
        }
    }
}
void getShadowedTable(Context *c)
{

    for (int i = 0; i < 257; i++) {
        if (!c->shadowed[i]) {
            c->shadowed[i] =
                i > 0 ? c->shadowed[i - 1] + c->t[i] - c->t[i - 1] : c->t[i] - c->t[i - 1];
        } else {
            c->shadowed[i] = i > 0 ? c->shadowed[i - 1] : 0;
        }
    }
    c->t = c->shadowed;
}

int findTable(Context *c)
{
    Table *cur = &c->table_[0];
    for (int i = 0; i < 257; i++) {
        c->shadowed[i] = 0;
    }
    cur = &c->table_[1];
    // printRecoursive(cur);
    for (int i = 0; i < 3 && c->context[i] != -1; i++) {
        if (cur->next[c->context[i]]) {
            cur = cur->next[c->context[i]];
        } else {
            cur->next[c->context[i]] = calloc(1, sizeof(c->table_[0]));
            cur->next[c->context[i]]->prev = cur;
            cur = cur->next[c->context[i]];
            cur->t[256] = 1;
            cur->maxDiv = cur->prev->maxDiv;
            cur->aggressive = cur->prev->aggressive;
        }
    }
    for (int i = 0; i < c->backInd; i++) {
        for (int j = 0; j < 256; j++) {
            if ((*(cur->t + j) - *(cur->t + j - 1))) {
                c->shadowed[j] = 1;
            }
        }
        if (cur->prev) {
            cur = cur->prev;
        } else {
            c->table = cur;
            c->t = c->table->t;
            // printRecoursive(&c->table_[1]);

            return -1;
        }
    }
    c->table = cur;
    c->t = c->table->t;

    // printRecoursive(&c->table_[1]);
    return 0;
}

static void updateTable2(Context *c)
{
    int byte_in = c->byte_in;
    findTable(c);
    // for (int i = 0; i < 257; i++) {
    //     printf("%lu ", c->t[i]);
    // }
    // printf("\n");
    getShadowedTable(c);
    while (c->t[byte_in] == c->t[byte_in - 1] || (byte_in == 256 && c->table != &c->table_[0])) {
        // for (int i = 0; i < 257; i++) {
        //     printf("%lu ", c->t[i]);
        // }
        // printf("\n");
        c->byte_in = 256;
        getInInterval(c);
        c->table->missed++;
        c->backInd++;
        findTable(c);
        // for (int i = 0; i < 257; i++) {
        //     printf("%lu ", c->t[i]);
        // }
        // printf("\n");
        getShadowedTable(c);

        // printRecoursive(&c->table_[1]);
        // exit(1);
    }
    c->byte_in = byte_in;
    getInInterval(c);
    c->table->missed--;
    c->backInd = 0;
}

static void updateTable(Context *c)
{
    Table *cur = &c->table_[0];
    if (cur->t[c->byte_in] - *(cur->t + c->byte_in - 1)) {
        for (int i = c->byte_in; i < c->tableSize; i++) {
            cur->t[i]--;
        }
    }
    // printf("1\n");
    // printRecoursive(&c->table_[1]);
    cur = &c->table_[1];
    for (int i = c->byte_in; i < c->tableSize; i++) {
        cur->t[i] += cur->aggressive;
    }
    if (cur->missed > 1) {
        cur->t[256] = cur->t[255] + (cur->t[255] / 2 ? cur->t[255] / 2 : 1);
    } else {
        cur->t[256] = cur->t[255] + 1;
    }
    // cur->t[256] = cur->t[255] + (cur->t[255] / 2 ? cur->t[255] / 2 : 1);
    if (cur->t[c->tableSize - 1] >= cur->maxDiv) {

        uint64_t add = 0;
        for (int i = 0; i < c->tableSize; i++) {
            cur->t[i] >>= 1;
            cur->t[i] += add;
            if (cur->t[i] == *(cur->t + i - 1)) {
                cur->t[i]++;
                add++;
            }
        }
    }
    for (int i = 0; i < 3 && c->context[i] != -1; i++) {

        cur = cur->next[c->context[i]];
        for (int i = c->byte_in; i < c->tableSize; i++) {
            cur->t[i] += cur->aggressive;
        }
        if (cur->missed > 1) {
            cur->t[256] = cur->t[255] + (cur->t[255] / 2 ? cur->t[255] / 2 : 1);
        } else {
            cur->t[256] = cur->t[255] + 1;
        }
        // cur->t[256] = cur->t[255] + (cur->t[255] / 2 ? cur->t[255] / 2 : 1);
        if (cur->t[c->tableSize - 1] >= cur->maxDiv) {

            uint64_t add = 0;
            for (int i = 0; i < c->tableSize; i++) {
                cur->t[i] >>= 1;
                cur->t[i] += add;
                if (cur->t[i] == *(cur->t + i - 1)) {
                    cur->t[i]++;
                    add++;
                }
            }
        }
    }

    // printf("2\n");
    // printRecoursive(&c->table_[1]);

    for (int i = 2; i > 0; i--) {
        c->context[i] = c->context[i - 1];
    }
    c->context[0] = c->byte_in;
}

static void initContext(Context *c)
{
    uint64_t MAXDIV = ((uint64_t)1 << 12);
    uint64_t AGGRESSIVE = 1;
    c->ONE = ((uint64_t)1 << 32);
    c->HALF = (c->ONE / 2);
    c->FIRST_QTR = (c->ONE / 4);
    c->THIRD_QTR = (c->FIRST_QTR * 3);
    c->tableSize = sizeof(c->table_[0].t) / sizeof(c->table_[0].t[0]);
    c->tableCount = c->tableSize * c->tableSize;
    c->table_ = calloc(c->tableCount, sizeof(c->table_[0]));
    c->table = &c->table_[0];
    c->t = c->table->t;
    c->a = 0;
    c->b = c->ONE - 1;
    Table *table = &c->table_[0];
    table->maxDiv = MAXDIV;
    table->aggressive = AGGRESSIVE;
    for (int i = 0; i < c->tableSize; i++) {
        table->t[i] = (i + 1);
        // c->table[i] = (i + 1); //* (MAXDIV) / tableSize; //- 2 * ((i + 1) % 2);
        // printf("%x %lu ", i, table[i]);
    }

    table = &c->table_[1];
    table->maxDiv = MAXDIV;
    table->aggressive = AGGRESSIVE;
    table->t[256] = 1;
    table->prev = &c->table_[0];

    for (int i = 2; i >= 0; i--) {
        c->context[i] = -1;
    }
    c->tableCountI = 2;
}
void compress_ppm(char *ifile, char *ofile)
{
    Context c_ = {0};
    Context *c = &c_;
    if (mopen(&c->files, ifile, ofile)) {
        return;
    }
    initContext(c);

    int in;
    while ((in = getByte(&c->files)) != -1) {
        c->byte_in = in;
        updateTable2(c);
        // printf("%c", c->byte_in);
        updateTable(c);
    }
    c->byte_in = 256;
    // printf("%c", c->byte_in);
    updateTable2(c);
    updateTable(c);
    if (c->a != 0) {
        add_bits_plus_follow(c, 1);
    } else {
        add_bits_plus_follow(c, 0);
    }
    printf("%g %g\n", c->a / (double)c->ONE, c->b / (double)c->ONE);

    mclose(&c->files);
    freeRecoursive(&c->table_[1]);
    free(c->table_);
}

void decompress_ppm(char *ifile, char *ofile)
{

    Context c_ = {0};
    Context *c = &c_;
    if (mopen(&c->files, ifile, ofile)) {
        return;
    }
    c->decompress = 1;
    initContext(c);
    int in = 0;
    if (in != -1) {
        for (int i = 8; c->ONE >> i > 0 && in != -1; i += 8) {
            in = getByte(&c->files);
            if (in != -1) {
                c->val |= (uint64_t)in * (c->ONE >> i);
            }
        }
        for (;;) {
            printf("table %ld\n", (c->table - c->table_) / sizeof(c->table));
            if (findTable(c) == -1) {
                break;
            }
            getShadowedTable(c);
            int dec = getByteFromValue(c);
            if (dec == -1) {
                break;
            }
            printf("read %d\n", dec);
            if (dec == 256) {
                c->table->missed++;
                c->backInd++;
            } else {
                c->table->missed--;
                c->backInd = 0;
            }
            c->byte_in = dec;
            getInInterval(c);
            if (c->byte_in != 256) {
                updateTable(c);
                writeByte(&c->files, c->byte_in);
            }
        }
    }

    mclose(&c->files);
    freeRecoursive(&c->table_[1]);
    free(c->table_);
}
