#pragma once
#include <inttypes.h>
#include <stdio.h>
typedef struct FILE_IO_
{
    FILE *fi;
    FILE *fo;
    uint64_t buff_len;
    uint64_t buff_i;
    uint64_t buff_i_bit;
    uint8_t buff[64000];
    uint64_t buff2_i;
    uint64_t buff2_i_bit;
    uint8_t buff2[64000];
} FILE_IO;
void readTable(FILE_IO *c, uint64_t *table);
void getbestTable(FILE_IO *c, uint64_t *table);
int getBit(FILE_IO *c);
int getByte(FILE_IO *c);
int mopen(FILE_IO *c, char *ifile, char *ofile);
int mclose(FILE_IO *c);
uint64_t getfileLen(FILE *f);
void mflush(FILE_IO *c);
void writeByte(FILE_IO *c, uint8_t byte);
void writeBit(FILE_IO *c, int bit);